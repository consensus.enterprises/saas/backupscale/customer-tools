# BackUpScale Device Set-Up

This is an [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)) role for configuring backup sources to be backed up by the [BackUpScale](https://backupscale.com/) service.

## Overview

This Ansible role performs the following operations, in order:

1. Verifies prerequisites
1. Sets the Restic password (as prompted from the playbook)
1. Accepts the service's SSH host keys
1. Installs Backrest
1. Provides the documentation URL to help finish configuring your backup source

## Requirements

* Any device that can run Ansible.

## Supported OSes

Merge requests or funding for any non-working OSes are welcome and appreciated.

## Role Variables

See [defaults/main.yml](https://gitlab.com/backupscale/ansible-collection/-/blob/master/roles/backup_source/defaults/main.yml) for all role variables.

## Dependencies

* Ansible

## Example Playbook

See [playbooks/configure-backup-sources.yml](https://gitlab.com/backupscale/ansible-collection/-/blob/master/playbooks/configure-backup-sources.yml).

This can typically be run on the client from the CLI like so:

```sh
ansible-playbook --inventory server1,server2,server3 /path/to/this/repository/playbooks/configure-backup-sources.yml
```

... or locally:

```sh
ansible-playbook --inventory localhost, /path/to/this/repository/playbooks/configure-backup-sources.yml
```

## Issue Tracking

For bugs, feature requests, etc., please visit the [issue tracker](https://gitlab.com/backupscale/ansible-collection/-/boards).

## License

GNU AGPLv3

## Author Information

Written by [Colan Schwartz](https://colan.pro/) at [BackUpScale](https://backupscale.com/).  To contact us, please see our [Community page](https://backupscale.com/community/).
