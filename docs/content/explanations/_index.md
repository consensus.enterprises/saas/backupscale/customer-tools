---
title: "Explanations"
date: 2024-12-11T12:47:10-04:00
weight: 4

---

<!---
[EXPLANATION](https://diataxis.fr/explanation/) IS DISCUSSION THAT CLARIFIES AND ILLUMINATES A PARTICULAR TOPIC. EXPLANATION IS UNDERSTANDING-ORIENTED.
-->

This is where we will put extra information, our considerations and architectural decisions. Why did we pick a particular sofware for a piece of this system?

Let us know if there's anything you'd like us to add here.
