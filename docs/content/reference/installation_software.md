---
title: "Installation Software"
date: 2024-12-11T12:47:10-04:00
weight: 1

---

This is all of the software that would typically get installed on you backup sources.  It's open source, exclusively, so it can be reviewed and audited.

### Ansible

* What it is: An automation tool 
* Required: No, but it automates the installation process by running our playbook (see below).
* Web site: https://www.ansible.com/
* Code: https://github.com/ansible/ansible/

### BackUpScale Ansible Playbook & Role

* What these are: Code that's run by Ansible to set up backup sources
* Required: No, but it automates the installation process. The playbook runs the role with the chosen parameters that you can modify, and then run your own. You can alternatively run the commands within the role yourself.
* Web site: https://galaxy.ansible.com/ui/namespaces/backupscale/  
* Code: https://gitlab.com/backupscale/customer-tools/-/tree/master?ref_type=heads

### Backrest

* What it is: The scheduler and Web-based front-end to Restic (see below)
* Required: No, but we recommend it because it simplifies tasks that would otherwise need to be done manually via the command-line interface (CLI).
* Web site: https://garethgeorge.github.io/backrest/ 
* Code: https://github.com/garethgeorge/backrest

### Restic

* What it is: The main backup tool used by the system
* Required: Generally, yes.  It may by possible to use [Rustic](https://rustic.cli.rs/) instead, but we don't officially support it.
* Web site: https://restic.net/
* Code: https://github.com/restic/restic/
