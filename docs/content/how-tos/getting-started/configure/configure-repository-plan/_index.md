---
title: "Repository Scheduling Configuration"
date: 2024-12-11T12:47:10-04:00
weight: 3

---

1. **Plan Name:** Enter whatever you'd like here; this is for identifying your plan in this user interface.
2. **Repository:** Select one of the repositories you created in the previous section.
3. **Paths:** Enter the paths on your device that you'd like backed up.
4. **Excludes:** Case-sensitive paths to exclude from your backups that are contained within the above paths. See [the Restic documentation](https://restic.readthedocs.io/en/stable/040_backup.html#excluding-files) for details.
5. **Excludes (Case Insensitive):** Same as above, but the paths aren't case sensitive. See [the Restic documentation](https://restic.readthedocs.io/en/stable/040_backup.html#excluding-files) for details.
6. **Backup Schedule:** The backup schedule.  You can hover over each option for in-line help.
![Backup schedule in-line help](backup-schedule.png)
7. **Backup Flags:** Set any options you'd like to add to the *backup* command.  For example, we recommend [not creating new snapshots if the files haven't changed](https://restic.readthedocs.io/en/stable/040_backup.html#skip-creating-snapshots-if-unchanged):
    * `--skip-if-unchanged`
8. **Retention Policy:** Set this to *None* because we'll normally be working with append-only repositories, which don't allow marking snapshot removals.  Instead, we'll handle this via out-of-band operations, where we temporarily disable append-only mode.
9. **Hooks:** You can hook into operations run here.  See [Hook Details](https://garethgeorge.github.io/backrest/docs/hooks) for more information.
10. **Preview:** This is simply for reviewing how the the above configuration will be stored, which can be helpful for automation.
11. Hit the **Submit** button to save the form.
12. After your Plan has been created, while on the Plan, hit the *Backup Now* button to start your first backup and test you configuration.
> [!NOTE]
> If you're backing up lots of data (either lots of files and/or big files), it may take a while for your backup task to complete.  After your first backup, however, there will be fewer changes to upload so backup operations will be quicker.
