---
title: "Backup Source Configuration"
date: 2024-12-11T12:47:10-04:00
weight: 4

---

We'll be configuring the set-up in our recommended management console, [Backrest](https://garethgeorge.github.io/backrest/), which was installed in the previous section.  For additional documentation not provided here, please review [the Backrest documentation](https://garethgeorge.github.io/backrest/introduction/getting-started).

1. [Configure management console](configure-management-console)
2. [Configure repository instance](configure-repository-instance)
3. [Configure repository plan](configure-repository-plan)
